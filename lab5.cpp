/*
Hannah Fowler
hgfowle
Lab 3
Section 001
Nushrat Humaira
*/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;

enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
	Suit suit;
	int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}

int main(int argc, char const *argv[]) {
	// IMPLEMENT as instructed below
	/*This is to seed the random generator */

	srand(unsigned (time(0)));

	/*Create a deck of cards of size 52 (hint this should be an array) and
	*initialize the deck*/

	Card deck[52] = { };
	int i;
	
	deck[0].value = 2;

	for (i = 0; i < 13; i++) {
		deck[i].suit = SPADES;
		deck[i].value = i + 2;
	}
	for (i = 13; i < 26; i++) {
		deck[i].suit = HEARTS;
		deck[i].value = (i - 13) + 2;
	}
	for (i = 26; i < 39; i++) {
		deck[i].suit = DIAMONDS;
		deck[i].value = (i - 26) + 2;
	}
	for (i = 39; i < 52; i++) {
		deck[i].suit = CLUBS;
		deck[i].value = (i - 39) + 2;
	}

	/*After the deck is created and initialzed we call random_shuffle() see the
	*notes to determine the parameters to pass in.*/

	random_shuffle(&deck[0], &deck[52], myrandom);

	/*Build a hand of 5 cards from the first five cards of the deck created
	*above*/

	Card hand[5];

	for (i = 0; i < 5; i++) {
		hand[i] = deck[i];
	}

	/*Sort the cards.  Links to how to call this function is in the specs
	*provided*/

	sort(&hand[0], &hand[5], suit_order);

	/*Now print the hand below. You will use the functions get_card_name and
	*get_suit_code */

	for (i = 0; i < 5; i++) {
		cout << setw(10) << right << get_card_name(hand[i]) << " of " << get_suit_code(hand[i]) << endl;
		
	}

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
	if (lhs.suit < rhs.suit) {
		return true;
	}
	else if ((lhs.suit == rhs.suit) && (lhs.value < rhs.value)) {
		return true;
	}
	else {
		return false;
	}
}

string get_suit_code(Card& c) {
	switch (c.suit) {
		case SPADES:    return "\u2660";
		case HEARTS:    return "\u2661";
		case DIAMONDS:  return "\u2662";
		case CLUBS:     return "\u2663";
		default:        return "";
	}
}

string get_card_name(Card& c) {
	switch (c.value) {
		case 11:	return "Jack";
		case 12:	return "Queen";
		case 13:	return "King";
		case 14:	return "Ace";
		default:	return to_string(c.value);
	}
}

